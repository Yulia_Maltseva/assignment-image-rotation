#include "../include/transform.h"
#include <malloc.h>


static size_t result_position(size_t i, size_t j, size_t height){
    return height * j + (height - i - 1);
}

static size_t image_position(size_t i, size_t j, size_t width){
    return i * width + j;
}

struct image transform(const struct image *image) {

    if (!image->data) {
        return (struct image) {.width = image->width, .height = image->height, .data = NULL};
    }

    struct pixel *data = malloc(sizeof(struct pixel) * image->width * image->height);
    
    for (size_t i = 0; i < image->height; ++i) {
        for (size_t j = 0; j < image->width; ++j) {
            data[result_position(i, j, image->height)] = image->data[image_position(i,j,image->width)];
        }
    }

    return (struct image) {.width = image->height, .height = image->width, .data = data};
      
}
