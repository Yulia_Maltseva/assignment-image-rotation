#include "../include/bmp.h"
#include "stdint.h"
#include <stdbool.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE *file, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}

static size_t size_of_file(const size_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

static size_t get_padding_size(size_t width){
    return width % 4;
}

static size_t size_of_image(const struct image *image) {
    return image->height * (3 * image->width + get_padding_size(image->width));
}

enum status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
        return READ_ERROR;
    }

    *image = image_create(header.biWidth, header.biHeight);

    const size_t size_per_row = (size_t)(image->width) * sizeof(struct pixel);
    
    for (size_t i = 0; i < image->height; i++) {
    	if(fread(image->data + i * (image->width), size_per_row, 1, in) != 1) {
            image_destroy(*image);
            return READ_ERROR;
    	}

    	if (fseek(in, (long) get_padding_size(image->width), SEEK_CUR)) {//указатель с текущей позиции
            image_destroy(*image);
            return READ_ERROR;
        }
    }
    
    return SUCCESS;
}

static struct bmp_header create_bmp_header(const struct image* image) {
    const uint64_t image_size = size_of_image(image);
    struct bmp_header header = {
        .bfType = 19778,
        .bfileSize = size_of_file(image_size),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
    return header;
}

enum status to_bmp(FILE *out, const struct image *image) {

	struct bmp_header header = create_bmp_header(image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    fseek(out, header.bOffBits, SEEK_SET);

    const uint8_t zero = 0;
    
    size_t i = 0;
    size_t j = 0;
    
    if (image->data) {
        while (i < image->height){
        	fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out);
        	while (j < get_padding_size(image->width)){
        		if(fwrite(&zero, 1, 1, out) != 1) return WRITE_ERROR;
        		j++;
        	}
       	i++;
        j=0;
        }      
    } else {
        return WRITE_ERROR;
    }

    return SUCCESS;
}
