#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/status.h"
#include "../include/transform.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if (argc != 3) {
        fprintf(stderr, "You need to mention: ./image-transformer link_to_origrnal new_link");
        return -1;
    }

    FILE *input_file = {0};
    struct image first_image = (struct image){0};
    FILE *output_file = {0};

    enum status status = file_open(argv[1], &input_file, "r");
    if (status != SUCCESS) {
        fprintf( stderr,"Не получилось открыть файл!");
        return 1;
    }

    status = from_bmp(input_file, &first_image);
    if (status != SUCCESS) {
        fprintf( stderr,"Ошибка в чтении файла!");
        return 1;
    }

    status = file_close(input_file);
    if (status != SUCCESS) {
        fprintf( stderr,"Не получилось закрыть файл!");
        return 1;
    }

    struct image second_image = transform(&first_image);
    image_destroy(first_image);

    status = file_open(argv[2], &output_file, "w");
    if (status != SUCCESS) {
        fprintf( stderr,"Не получилось открыть файл!");
        return 1;
    }
    status = to_bmp(output_file, &second_image);
    if (status != SUCCESS) {
        fprintf( stderr,"Ошибка записи в файл!");
        return 1;
    }
    status = file_close(output_file);
    if (status != SUCCESS) {
        fprintf( stderr,"Не получилось закрыть файл!");
        return 1;
    }
    image_destroy(second_image);
    fprintf( stdout,"Успешно");

    return 0;

}
