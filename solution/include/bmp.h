#ifndef BMP_H
#define BMP_H


#include "../include/image.h"
#include <malloc.h>
#include <status.h>
#include  <stdint.h>

enum status from_bmp(FILE *in, struct image *image);

enum status to_bmp(FILE *out, const struct image *image);

#endif
